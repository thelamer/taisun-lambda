'use strict';
var AWS = require('aws-sdk');
var dynamodb = new AWS.DynamoDB();
var route53 = new AWS.Route53();
var uuid = require('uuid');
var https = require('https');
var responseCode = 200;
var sitekey = HIDDEN;
var zoneid = HIDDEN;
var tld = '.taisun.io';
exports.handler = function(event, context, callback) {
    var sourceip = event.requestContext.identity.sourceIp;
    var captcharesponse = event.body.replace('g-recaptcha-response=','');
    var url = 'https://www.google.com/recaptcha/api/siteverify?secret=' + sitekey + "&response=" + captcharesponse;
    // Send captcha response to google for validation
    https.get(url, function(res) {
        var body = '';
        res.on('data', function(d) {
            body += d;
        });
        res.on('end', function() {
            var parsed = JSON.parse(body);
            // The captcha succeeded
            if (parsed.success == true){
                // Generate the unique url and domain key needed for the update api (https://api.taisun.io/production/dyndns?key=<dnskey>)
                var dnskey = uuid().replace(/-/g, '');
                var domain = uuid().replace(/-/g, '');
                var dynamoparams = {
                 Item: {
                  "guid": {
                    S: dnskey
                   }, 
                  "domain": {
                    S: domain
                   }, 
                  "ip": {
                    S: sourceip
                   }
                 }, 
                 ReturnConsumedCapacity: "TOTAL", 
                 TableName: "dyndns"
                 };
                // Insert record in dynamodb
                dynamodb.putItem(dynamoparams, function(err, data) {
                  if (err) {console.log(err, err.stack);}
                  else {
                    // Create the route53 entry
                      var params53 = {
                        ChangeBatch: {
                          Changes: [{
                            Action: "CREATE",
                            ResourceRecordSet: {
                              Name: '*.' + domain + tld,
                              ResourceRecords: [{
                                Value: sourceip
                              }],
                              TTL: 60,
                              Type: "A"
                            }
                          }]
                        },
                        HostedZoneId: zoneid
                      };
                      route53.changeResourceRecordSets(params53, function(err, data) {
                        if (err) {console.log(err, err.stack);}
                        else{
                          var body = '\
<!DOCTYPE html>\
<html>\
<head>\
  <title>Taisun URL Generated</title>\
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" rel="stylesheet">\
</head>\
  <body style="padding-top:40px;padding-bottom:40px;background-color:#eee;">\
    <div class="card" style="max-width:800px;padding:15px;margin:0 auto;">\
      <div class="card-block">\
        <h2>URL Generated:</h2>\
        <table class="table table-bordered">\
          <tr><td>Domain</td><td>*.' + domain + '.taisun.io</td></tr>\
          <tr><td>DNSKey</td><td>' + dnskey + '</td></tr>\
          <tr><td>Update</td><td><a href="https://api.taisun.io/production/dyndns?key=' + dnskey + '" target="_blank">https://api.taisun.io/production/dyndns?key=' + dnskey + '</a></td></tr>\
          <tr><td>Current IP</td><td>' + sourceip + '</td></tr>\
        </table>\
        <p>Run updater in Docker:</p>\
        <div class="card bg-faded">\
          <pre>docker run --name taisundns -d -e DNSKEY=' + dnskey + ' taisun/dyndns</pre>\
        </div><br>\
        <p>Add cron job to system:(requires curl)</p>\
        <div class="card bg-faded">\
          <pre>(crontab -l 2>/dev/null; echo "*/30 * * * * curl --insecure --silent https://api.taisun.io/production/dyndns?key=' + dnskey + ' > /dev/null") | crontab -</pre>\
        </div><br>\
        <p>Manual curl cli:</p>\
        <div class="card bg-faded">\
          <pre>curl --insecure https://api.taisun.io/production/dyndns?key=' + dnskey + '</pre>\
        </div><br>\
        <p>Manual wget cli:</p>\
        <div class="card bg-faded">\
          <pre>wget --no-check-certificat -qO- https://api.taisun.io/production/dyndns?key=' + dnskey + '</pre>\
        </div><br>\
      </div>\
    </div>\
  </body>\
</html>\
                          ';
                            var response = {
                              statusCode: responseCode,
                              headers: {
                                'Content-Type': 'text/html',
                              },
                              body: body
                            };
                            callback(null, response);
                        }
                      });
                  }
                });
            }
            // The captcha failed
            else {
                var response = {
                  statusCode: responseCode,
                  headers: {
                    'Content-Type': 'text/html',
                  },
                  body: '<h1>Error: Captcha Failed Verification</h1>'
                };
                callback(null, response);  
            }
        });
    });
};
