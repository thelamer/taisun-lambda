import socket
import json
def lambda_handler(event, context):
  if event['queryStringParameters'] is not None:
    if 'host' in event['queryStringParameters']:
      host = event['queryStringParameters']['host']
      # Make sure this is a subdomain
      if len(host.split('.')) == 3:
        # Make sure this is for a taisun.io domain
        if host.split('.')[1] + '.' + host.split('.')[2] == 'taisun.io':
          sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
          sock.settimeout(3)
          host = 'taisun-gateway.' + host
          try:
            result = sock.connect_ex((host,4443))
            if result == 0:
              port = 'open'
              message = host + ' is accesible on 4443 externally'
            else:
              port = 'closed'
              message = host + ' is not accesible on 4443 externally'
          except socket.error as msg:
            port = 'error'
            message = 'socket error confirm this domain exists'
        else:
          port = 'error'
          message = 'This is not a taisun.io domain'
      else:
        port = 'error'
        message = 'This is not a subdomain'
    else:
      port = 'error'
      message = 'host is a required parameter'
  else:
    port = 'error'
    message = 'host is a required parameter'
  # Send results to response
  responseCode = 200
  res = {'result':port,'message':message}
  response = {
    'statusCode': responseCode,
    'body': json.dumps(res, indent=4, sort_keys=True)
  }
  return response
