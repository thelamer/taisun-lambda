import json
import requests
import pymysql
import uuid
from os import environ
def lambda_handler(event, context):
  # Main Mysql Connect
  db=pymysql.connect(
  host=environ.get('MYSQL_URL'),
  user=environ.get('MYSQL_USER'),
  port=3306,
  passwd=environ.get('MYSQL_PASS'),
  db=environ.get('MYSQL_DATABASE'),
  autocommit=True)
  badauth = '?user=unauthorized'
  # Make sure we got a code back from github
  if event['queryStringParameters'] is not None:
    if 'code' in event['queryStringParameters']:
      # Use the temp code from oauth to ping the github API
      code = event['queryStringParameters']['code']
      url = 'https://github.com/login/oauth/access_token'
      payload = {
          'client_id': environ.get('CLIENT_ID'),
          'client_secret': environ.get('CLIENT_SECRET'),
          'code': code
      }
      print(payload)
      headers = {'Accept': 'application/json'}
      r = requests.post(url, params=payload, headers=headers)
      response = r.json()
      # If this is a legit code and response grab the username
      if 'access_token' in response:
        userurl = 'https://api.github.com/user?access_token=' + response['access_token']
        r = requests.get(userurl)
        userinfo = r.json()
        # If the response contains a username
        if 'login' in userinfo:
          user = userinfo['login']
          cur = db.cursor()
          cur.execute('select name,guid from users where name = %s',(user,))
          # If this is a new user generate an API key for them
          if cur.rowcount == 0:
            userguid = str(uuid.uuid4())
            cur.execute('insert into users (name,guid) values (%s,%s)',(user,userguid,))
            cur.close()
            res = '?user=' + user + '&apikey=' + userguid
          # Simply return the users api key
          else:
            taisunuser = cur.fetchone()
            cur.close()
            res = '?user=' + taisunuser[0] + '&apikey=' + taisunuser[1]
        else:
          res = badauth        
      else:
        res = badauth
    else:
      res = badauth
  else:
    res = badauth
  # Close DB Connection
  db.close()
  # Send a redirect response so the JS client app for stacks.taisun.io can be used to manage stacks
  responseCode = 302
  response = {
    'statusCode': responseCode,
    'headers': {
        'Location': 'https://stacks.taisun.io/' + res
    }
  }
  return response
