'use strict';
var AWS = require('aws-sdk');
var dynamodb = new AWS.DynamoDB();
var route53 = new AWS.Route53();
var zoneid = process.env.ZONEID;
var tld = process.env.TLD;
exports.handler = function(event, context, callback) {
  var sourceip = event.requestContext.identity.sourceIp;
  var responseCode = 200;
  if (event.queryStringParameters !== null && event.queryStringParameters !== undefined) {
    if (event.queryStringParameters.key !== undefined && event.queryStringParameters.key !== null && event.queryStringParameters.key !== "" && event.queryStringParameters.txt !== undefined && event.queryStringParameters.txt !== null && event.queryStringParameters.txt !== "") {
      var dynamoparams = {
        Key: {
          "guid": {
            S: event.queryStringParameters.key
          }
        },
        TableName: "dyndns"
      };
      dynamodb.getItem(dynamoparams, function(err, data) {
        if (err) {
          console.log(err, err.stack);
        }
        else {
          if (data.Item == null) {
            var responseBody = {
              sourceip: sourceip,
              error: event.queryStringParameters.key + ' is not a valid domain dnskey'
            };
            respond(responseBody);
          }
          else {
            var txt = event.queryStringParameters.txt;
            var subdomain = data.Item.domain.S;
            var params53 = {
              ChangeBatch: {
                Changes: [{
                  Action: "UPSERT",
                  ResourceRecordSet: {
                    Name: '_acme-challenge.' + subdomain + tld,
                    ResourceRecords: [{
                      Value: '"' + txt + '"'
                    }],
                    TTL: 60,
                    Type: "TXT"
                  }
                }]
              },
              HostedZoneId: zoneid
            };
            route53.changeResourceRecordSets(params53, function(err, data) {
              if (err) {
                console.log(err, err.stack);
              }
              else {
                var responseBody = {
                  sourceip: sourceip,
                  action: 'updated',
                  message: 'The subdomain ' + subdomain + '.taisun.io set a txt record for ' + txt
                };
                respond(responseBody);
              }
            });
          }
        }
      });
    }
    else {
      var responseBody = {
        sourceip: sourceip,
        action: 'none',
        message: 'Parameter passed not recognized this endpoint requires a ?key=<yourdnskey> parameter and ?txt=<yourtxtrecord> parameter'
      };
      respond(responseBody);
    }
  }
  else {
    var responseBody = {
      sourceip: sourceip,
      action: 'none',
      message: 'No parameters passed this endpoint requires a ?key=<yourdnskey> parameter and ?txt=<yourtxtrecord> parameter'
    };
    respond(responseBody);
  }

  function respond(responseBody) {
    console.log(responseBody);
    var response = {
      statusCode: responseCode,
      body: JSON.stringify(responseBody, null, 4)
    };
    callback(null, response);
  }
};

